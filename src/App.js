import { useState } from 'react';
import { ethers } from 'ethers';
import './App.css';

const App = () => {
  const [senderAccountAddress, setSenderAccountAddress] = useState('');
  const [senderAccountBalance, setSenderAccountBalance] = useState('');
  const [amountToSend, setAmountToSend] = useState('');
  const [toAccountAddress, setToAccountAddress] = useState('');

  const connectWallet = async () => {
    try {
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const accounts = await window.ethereum.request({method: 'eth_requestAccounts',});
      let balance = await provider.getBalance(accounts[0]);
      let balanceEth = ethers.utils.formatEther(balance);
      setSenderAccountAddress(accounts[0]);
      setSenderAccountBalance(balanceEth);
    } catch (error) {
      alert(error);
    }
  };
  
  const submitHandler= async ()=>{
    await window.ethereum.request(
      {
        method:"eth_sendTransaction",
        params: [
          {
            from : senderAccountAddress.toString(),
            to :toAccountAddress.toString(),
            value: ethers.utils.parseEther(amountToSend).toHexString(),
          }
        ]
      }
    );
    alert("Transaction Initiated");
  }

  if (!window.ethereum) {
    return <p>None ethereum browser detected</p>
  } else {
    return (
      <div className="App">
        <div className="App-header">
          {/* Wallet connection */}
          <div>
            <button className="button" onClick={connectWallet}>Connect Wallet</button>
            <p>
              Connected Wallet Address: { senderAccountAddress || "NA"}
              <br/>
              Balance: { senderAccountBalance || "NA"}
            </p>
          </div>
          {/* Transaction Form */}
          <div>
            <div>
              <h4>Send Eth to Someone</h4>
              <input
                className="input"
                type="text"
                placeholder="Enter Wallet Address"
                onChange={(e)=> setToAccountAddress(e.target.value)}
              />
              <br/>
              <input
                className="input"
                type="number"
                placeholder="Enter eth Amount"
                onChange={(e) => setAmountToSend(e.target.value)}
              />
              <br/>
              <button
                className="button"
                onClick={submitHandler}
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
